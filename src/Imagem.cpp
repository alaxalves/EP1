#include <iostream>
#include <string>
#include "Imagem.hpp"
#include <fstream>

using namespace std;

Imagem::Imagem(){
	
}
int Imagem::getAltura(){
	return altura;
}
void Imagem::setAltura(int altura){
	this->altura = altura;
}
int Imagem::getLargura(){
	return largura;
}
void Imagem::setLargura(int largura){
	this->largura = largura;
}
int Imagem::getPixels(){
	return num_max_pixels;
}
void Imagem::setPixels(int num_max_pixels){
	this->num_max_pixels = num_max_pixels;
}
string Imagem::getTipo(){
	return tipo;
}
void Imagem::setTipo(string tipo){
	this->tipo = tipo;
}
int Imagem::getPosicao(){
	return posicao;
}
void Imagem::setPosicao(int posicao){
	this->posicao = posicao;
}

void Imagem::leituraImagem(string local_img, Imagem * imgGeral){
		char hash;
		char hifen;
		int posicao = 0;
		int altura = 0;
		int largura = 0;
		string numero_magico; ///numero magico é o tipo da imagem, no caso será P5 ou P6
		string comentario;

		ifstream arquivo_img;
		arquivo_img.open(local_img, ios_base::binary);
		while(1){
			if(arquivo_img.is_open()){
				cout << endl << "Imagem aberta com sucesso" << endl << endl;

				break;
			}
			else {
				cout << endl << "Erro ao abrir a Imagem" << endl << endl;
				
				break;
		}
	}

	arquivo_img >> numero_magico;
	
	if(numero_magico == "P5"){
	//IMAGEM DO TIPO PGM
	arquivo_img >> hash >> posicao >> hifen >> comentario >> comentario >> comentario >> comentario >> altura >> largura >> num_max_pixels; 
	imgGeral->setPosicao(posicao);
	imgGeral->setTipo(numero_magico);
	imgGeral->setAltura(altura);
	imgGeral->setLargura(largura);
	imgGeral->setPixels(num_max_pixels);
	arquivo_img.close();
} else if (numero_magico == "P6"){
	// IMAGEM DO TIPO PPM
	arquivo_img >> comentario >> altura >> largura >> num_max_pixels; 
	imgGeral->setTipo(numero_magico);
	imgGeral->setAltura(altura);
	imgGeral->setLargura(largura);
	imgGeral->setPixels(num_max_pixels);
	arquivo_img.close();

}
}