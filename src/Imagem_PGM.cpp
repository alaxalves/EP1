#include <iostream>
#include "Imagem_PGM.hpp"
#include <fstream>
#include <string>

using namespace std;

Imagem_PGM::Imagem_PGM(){

}

void Imagem_PGM::decifra_PGM(string local, int pos_inicial){

	ifstream arquivo_img;
		arquivo_img.open(local, ios_base::binary);
		for(int contador = 0; contador < 4; contador ++){
			string linha;
			getline(arquivo_img, linha, '\n');
		}	

		arquivo_img.seekg(pos_inicial, ios_base::cur); 

		char mensagem_incompleta = 0, caracter = 0, ultimo_bit = 0, mensagem = 0 ;
		while(mensagem != '#'){
			mensagem = 0;
			for(int count = 0; count < 8; count ++){
				arquivo_img.get(caracter);
				ultimo_bit = 0x01 & caracter;
				if(count == 0){
					mensagem_incompleta = (ultimo_bit << 1);
					mensagem = mensagem | mensagem_incompleta;
				}
				else if (count == 7){
					mensagem_incompleta = (ultimo_bit);
					mensagem = mensagem | mensagem_incompleta;
				} 
				else {
					mensagem_incompleta = (ultimo_bit << 1);
					mensagem = (mensagem << 1) | mensagem_incompleta;
			}
			
		}
		cout << mensagem;
	}	

}	