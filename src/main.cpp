#include <iostream>
#include "Imagem.hpp"
#include <string>
#include <fstream>
#include "Imagem_PGM.hpp"
#include "Imagem_PPM.hpp"

using namespace std;

 int main(int argc, char ** argv){

 	Imagem * img = new Imagem();
 	string local_img;
 	string novo_local_img;
 	char filtroRGB;

 	cout << "Digite o local, nome e extensão da Imagem a ser decifrada: " << endl;
 	cin >> local_img;
 	//local do arquivo: /home/alaxalves/Documentos/EP1/doc
 	img->leituraImagem(local_img, img);

 	if(img->getTipo() == "P5"){

 		cout << "A Imagem selecionada é do tipo PGM" << endl;
 		cout << "----------------------------------" << endl;
 		Imagem_PGM * img_PGM = new Imagem_PGM();
 		img_PGM->setTipo(img->getTipo());
 		img_PGM->setAltura(img->getAltura());
 		img_PGM->setLargura(img->getLargura());
 		img_PGM->setPixels(img->getPixels());
 		img_PGM->setPosicao(img->getPosicao());
 		delete(img);
 		int posicao = img_PGM->getPosicao();
 		cout << endl << "MENSAGEM ESCONDIDA:" << endl;
 		img_PGM->decifra_PGM(local_img, posicao);
 		exit(1);

 	} else if (img->getTipo() == "P6"){

 		cout << "A Imagem selecionada é do tipo PPM" << endl;
 		cout << "----------------------------------" << endl << endl;
 		cout << "Digite o local da nova Imagem e depois ";
 		cout << "o nome e extensão do novo arquivo PPM:" << endl;
 		cin >> novo_local_img;
 		cout << "Escolha o filtro desejado: " << endl;
 		cout << "ex:" << endl << "digite: R, G ou B" << endl;
 		cin >> filtroRGB;

 		Imagem_PPM * img_PPM = new Imagem_PPM();
 		img_PPM->setTipo(img->getTipo());
 		img_PPM->setAltura(img->getAltura());
 		img_PPM->setLargura(img->getLargura());
 		img_PPM->setPixels(img->getPixels());

 		/*COnferindo 
 		cout << img_PPM->getAltura() << endl;
 		cout << img_PPM->getLargura() << endl;
 		cout << img_PPM->getTipo() << endl;
 		cout << img_PPM->getPixels() << endl;
		*/

 		delete(img);
 		cout << "Para ver a mensagem escondida abra a Imagem no local selecionado" << endl;
 		img_PPM->decifra_PPM(local_img, novo_local_img, img_PPM, filtroRGB);
 		exit(1);
 	}
 	
 }