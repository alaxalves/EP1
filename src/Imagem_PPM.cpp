
#include <iostream>
#include "Imagem_PPM.hpp"
#include <string>
#include <fstream>

using namespace std;

Imagem_PPM::Imagem_PPM(){
	
}

void Imagem_PPM::decifra_PPM(string local, string novo_local, Imagem_PPM * imgPPM, char filtro_RGB){
	string linhas;
	string comentario;
	char red;
	char green = 255;
	char blue = 255;

	ifstream abre_imgPPM;
	abre_imgPPM.open(local, ios_base::binary);

	for(int contador = 0 ; contador < 4; contador ++){
		getline(abre_imgPPM, linhas, '\n');
			if(contador == 1){
				comentario = linhas; //O que vem depois do hashtag
			}
	}

ofstream new_abre_imgPPM;
new_abre_imgPPM.open(novo_local, ios_base::binary);

new_abre_imgPPM << imgPPM->getTipo() << endl << comentario <<  endl << imgPPM->getAltura() << ' ' << imgPPM->getLargura() << endl << imgPPM->getPixels() << endl;

if(filtro_RGB == 'R'){
	for(int counter = 0 ; counter < imgPPM->getAltura() ; counter ++){
		for (int counter2 = 0 ; counter2 < imgPPM->getLargura() ; counter2 ++){
			abre_imgPPM.get(red);
			abre_imgPPM.get(green);
			abre_imgPPM.get(blue);
			//red = 0;
			green = 255;
			blue = 255;
			new_abre_imgPPM << red << green << blue;
		}
	}
	cout << "Filtro RED aplicado" << endl;
}
else if(filtro_RGB == 'G'){
	for(int counter = 0 ; counter < imgPPM->getAltura() ; counter ++){
		for (int counter2 = 0 ; counter2 < imgPPM->getLargura() ; counter2 ++){
			abre_imgPPM.get(red);
			abre_imgPPM.get(green);
			abre_imgPPM.get(blue);
			red = 255;
			//green = 0;
			blue = 255;
			new_abre_imgPPM << red << green << blue;

		}
	}
	cout << "Filtro GREEN aplicado" << endl;
}
else if(filtro_RGB == 'B'){
	for(int counter = 0 ; counter < imgPPM->getAltura() ; counter ++){
		for (int counter2 = 0 ; counter2 < imgPPM->getLargura() ; counter2 ++){
			abre_imgPPM.get(red);
			abre_imgPPM.get(green);
			abre_imgPPM.get(blue);
			red = 255;
			green = 255;
			//blue = 0;
			new_abre_imgPPM << red << green << blue;
		}
	}
	cout << "Filtro BLUE aplicado" << endl;
}

abre_imgPPM.close();
new_abre_imgPPM.close();
}