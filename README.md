

INSTRUÇÕES PARA COMPILAÇÃO:

- O caminho do arquivo deve ser digitado corretamente, em todas as suas etapas.

- A imagem PGM deve conter um cabeçalho de 4 linhas.
- No caso da imagem PGM:
	- A primeira linha deve conter o Tipo.	
			ex: P5
	- A segunda linha deve conter o Comentario que é iniciado por '#', seguido de um espaço e o numero que se inicia a mensagem oculta. 		Logo após deve conter um espaco seguido de um hifen seguido de outro espaco e por fim uma mensagem de 4 termos.
	- A terceira linha deve conter a altura e largura da imagem (nessa ordem).
	- A quarta linha deve conter o número máximo de pixels (por padrão, 255).
	- Na quinta linha se inicia o código da imagem de fato.

- O nome da imagem gerada (PPM) não pode ter o mesmo nome de outro arquivo no local selecionado.
	
INSTRUÇÕES PARA EXECUÇÃO

Passos:

1- No Terminal digite o caminho da pasta do Projeto. 
	ex: /home/alaxalves/Documentos/EP1

2- Para compilar digite 'make' no Terminal.

3- Para executar digite 'make run' no Terminal.

4- Digite o caminho da Imagem que você deseja decifrar.
	ex: /home/alaxalves/Documentos/EP1/doc/nomedaimagem.extensao

5- Na imagem PGM ao executar o passo 4 a mensagem escondida aparecerá automaticamente.

6- Na imagem PPM ao executar o passo 4 você deverá fazer o seguinte:

	- Digite o local da nova imagem com a mensagem já decifrada:
		ex: /home/alaxalves/Área\ de\ Trabalho/imagemDecifrada.ppm

	- Feito isso a Imagem PPM aparecerá com a mensagem decifrada no local selecionado.
