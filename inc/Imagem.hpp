#ifndef IMAGEM_HPP
#define IMAGEM_HPP

#include <string>
#include <iostream>
#include <fstream>

using namespace std;
class Imagem {

protected:

	int altura;
	int largura;
	int num_max_pixels;
	string tipo;
	int posicao;

public:
	Imagem();
	void setAltura(int altura);
	int getAltura();

	void setLargura(int largura);
	int getLargura();

	void setPixels(int num_max_pixels);
	int getPixels();

	void setTipo(string tipo);
	string getTipo();

	void setPosicao(int posicao);
	int getPosicao();

	void leituraImagem(string local_img, Imagem * imgGeral);


};
#endif