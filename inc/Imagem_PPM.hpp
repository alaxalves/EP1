#ifndef IMAGEM_PPM
#define IMAGEM_PPM

#include "Imagem.hpp"
#include <iostream>
#include <fstream>
#include <string>

//herança da classe Imagem
class Imagem_PPM : public Imagem{
public:
	
	Imagem_PPM();
	
	void decifra_PPM(string local, string novo_local, Imagem_PPM * imgPPM, char filtro_RGB);


};
#endif