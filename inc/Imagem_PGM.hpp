#ifndef IMAGEM_PGM_HPP
#define IMAGEM_PGM_HPP
#include "Imagem.hpp"

//herança da classe Imagem
class Imagem_PGM : public Imagem{
public:
	Imagem_PGM();
	void decifra_PGM(string local, int pos_inicial);

};
#endif